require('dotenv').config()
const net_snmp = require('net-snmp')
const alasql = require('alasql')

const bsnMobileStationTable = '1.3.6.1.4.1.14179.2.1.4'
const bsnAPTable = '1.3.6.1.4.1.14179.2.2.1'
const bsnAPReset = '1.3.6.1.4.1.14179.2.2.1.1.11'

const cLApTable = '1.3.6.1.4.1.9.9.513.1.1.1'

const controllerIp = process.env.CONTROLLERIP
const controllerCommunity = process.env.SNMPCOMMUNITY
const mode = process.env.MODE

const getbsnMobileStationTable = async (wifiControllerIp, communityString) => {
    return new Promise((resolve,reject)=>{
        const session = net_snmp.createSession (wifiControllerIp, communityString, {version: net_snmp.Version2c})
        const column = [1,2,3,4,7]
        session.tableColumns(
            bsnMobileStationTable,
            column,
            (error,table)=>{
                if (error) {
                    console.error(error)
                    reject(error)
                } else {
                    resolve(table)
                }
            }
        ) 
    })
}

const preparebsnMobileStationTable = async (table) => {    
    return new Promise((resolve,reject)=>{
        let result = []    
        for (const index in table) {
            result.push(
                {
                    macDec: index,
                    macHex: table[index][1]?table[index][1].toString('hex').replace(/(.{2})/g,"$1.").slice(0,-1):null,
                    ip: table[index][2]?table[index][2].toString('utf8'):null,
                    username: table[index][3]?table[index][3].toString('utf8'):null,
                    apMacHex: table[index][4]?table[index][4].toString('hex').replace(/(.{2})/g,"$1.").slice(0,-1):null,
                    ssid: table[index][7]?table[index][7].toString('utf8'):null,
                }
            )       
        }
        resolve(result)
    })
}

const getbsnAPTable = async (wifiControllerIp, communityString) => {
    return new Promise((resolve,reject)=>{
        const session = net_snmp.createSession (wifiControllerIp, communityString, {version: net_snmp.Version2c})
        const column = [1,3]
        session.tableColumns(
            bsnAPTable,
            column,
            (error,table)=>{
                if (error) {
                    console.error(error)
                    reject(error)
                } else {
                    resolve(table)
                }
            }
        ) 
    })
}

const preparebsnAPTable = async (table) => {
    return new Promise((resolve,reject)=>{
        let result = []
        const timestamp = Date.now()
        for (const index in table) {
            result.push(
                {
                    macDec: index,
                    apMacHex: table[index][1]?table[index][1].toString('hex').replace(/(.{2})/g,"$1.").slice(0,-1):null,
                    //apMacHex: index.split('.').map((e)=>{ return parseInt(e).toString(16) }).join('.'),
                    apName: table[index][3]?table[index][3].toString('utf8'):null,
                    timestamp: timestamp
                }
            )       
        }
        resolve(result)
    })
}

const getcLApTable = async (wifiControllerIp, communityString) => {
    return new Promise((resolve,reject)=>{
        const session = net_snmp.createSession (wifiControllerIp, communityString, {version: net_snmp.Version2c})
        const column = [1,5,6]
        session.tableColumns(
            cLApTable,
            column,
            (error,table)=>{
                if (error) {
                    console.error(error)
                    reject(error)
                } else {
                    resolve(table)
                }
            }
        ) 
    })
}

const preparecLApTable = async (table) => {
    return new Promise((resolve,reject)=>{
        let result = []
        for (const index in table) {
            result.push(
                {
                    macDec: index,
                    apMacHex: index.split('.').map((e)=>{ 
                        const octet = parseInt(e).toString(16)
                        if (octet.length===1) {
                            return `0${octet}`
                        } else {
                            return octet
                        }
                    }).join('.'),
                    apName: table[index][5]?table[index][5].toString('utf8'):null,
                    upTime: table[index][6]
                }
            )       
        }
        resolve(result)
    })
}

const resetAP = async (apMacDec,wifiControllerIp, communityString) => {
    return new Promise((resolve,reject)=>{
        const varbinds = [
            {
                oid: `${bsnAPReset}.${apMacDec}`,
                type: net_snmp.ObjectType.Integer,
                value: 1
            }
        ]
        const session = net_snmp.createSession (wifiControllerIp, communityString, {version: net_snmp.Version2c})
        session.set(varbinds, (error,varbinds)=>{
            if (error) {
                console.error(error)
                reject(error)
            } else {
                if (net_snmp.isVarbindError(varbinds[0])) {
                    reject(net_snmp.varbindError(varbinds[0]))
                } else {
                    resolve(varbinds[0])
                }
            }

        })

    })
}

const main = async () => {
    try {
        const userTable = await preparebsnMobileStationTable(await getbsnMobileStationTable(controllerIp,controllerCommunity))
        //userTable.map((e)=>{ console.log(e);})
        //const bnsApTable = await preparebsnAPTable(await getbsnAPTable(controllerIp,controllerCommunity))
        const cLApTable = await preparecLApTable(await getcLApTable(controllerIp,controllerCommunity))
        //console.log(cLApTable);
        const apTable = cLApTable

        const userCountTable = await alasql("SELECT apMacHex, COUNT(*) AS client FROM ? GROUP BY apMacHex",[userTable])
        const apTableWithClient = await alasql("SELECT * FROM ? AS ap LEFT JOIN ? AS user USING apMacHex",[apTable,userCountTable])
        const apTableWithOutClient = await alasql("SELECT * FROM ? WHERE client IS NULL",[apTableWithClient])
        //apTableWithClient.map((e)=>{ console.log(e);})
        //console.log(apTableWithOutClient.length, apTableWithClient.length);
        const resetedAp = apTableWithOutClient.filter((ap)=>{
            return ap.upTime > 60480000
        })
        .map(ap => {
            //console.log(e);
            if (mode==='reset') {
                resetAP(ap.macDec,controllerIp,controllerCommunity)    
            } else if(mode==='debug'){
                console.log(ap);
            }            
        })

        console.log(new Date(),controllerIp, `ApCount:${apTableWithClient.length}, noClient:${apTableWithOutClient.length}, reset:${resetedAp.length}`);

    } catch (error) {
        console.error(error)
    }
}
main()
