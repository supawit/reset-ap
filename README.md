built docker image
```
docker build . -t reset-ap
```
run image
```
docker run --rm -i -e CONTROLLERIP=<wlc controller mgmt ip> -e SNMPCOMMUNITY=<wlc snmp  read-write community string> -e MODE=<debug or reset> reset-ap
```